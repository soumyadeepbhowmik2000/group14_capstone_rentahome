# RENT A PLACE

## Great Learning's Final Capstone Project (G14-BATCH)

## Team Members

### 1. Aditya Tiwari<br> 2. Chetan Ramesh Kshirsagar<br> 3. Setti Sowmya<br> 4. Soumyadeep Bhowmik<br> 5. Tejashri Labhsetwar<br> 6. Uppalapati Sai Lakshmi Sahithi<br>

## Problem Statement: 

RentAPlace is place/property rental service.A full-stack web application using Spring Boot 2 and Angular 13. 
This is a Single Page Appliaction with client-side rendering. It includes backend and frontend two seperate projects on different branches.
The frontend client makes API calls to the backend server when it is running.

There are 2 users on the application: - <br> 1. User <br> 2. Admin

## Features
- JWT authentication
- Property Search
- Property Rating
- Checkin/Checkout
- Property Catalogue
- Q&A Service for customer query
- Booking Confirmation mailing service

## Technology Stacks
*Backend*
  - Java 11
  - Spring Boot 2.6
  - Spring Security
  - JWT Authentication
  - Spring Data JPA
  - Hibernate
  - SQL
  - Maven

*Frontend*
  - Angular 13
  - Angular CLI
  - Angular Material
  - Bootstrap

Modules:
1.Customer login
	- Create Customer from sign up page
2.Admin login
	- Default Admin Login
		User Name - admin@gmail.com
		Password - 1234

